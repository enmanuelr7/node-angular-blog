import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose/dist/mongoose.module';
import { ScheduleModule } from '@nestjs/schedule/dist/schedule.module';
import { BlogsModule } from './blogs/blogs.module';

if (process.env.NODE_ENV !== 'production') require('dotenv').config()

@Module({
  imports: [
    MongooseModule
      .forRoot(process.env.DATABASE_URL, { useFindAndModify: false, useCreateIndex: true }),
    BlogsModule,
    ScheduleModule.forRoot()
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
