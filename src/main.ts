import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BlogsService } from './blogs/blogs.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.get(BlogsService).saveBatch();
  app.enableCors();
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
