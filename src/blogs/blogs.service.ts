import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Interval } from '@nestjs/schedule/dist/decorators/interval.decorator';
import { Model } from 'mongoose';
import { Blog, BlogDocument } from './schemas/blog.schema';
import { map } from 'rxjs/operators';

@Injectable()
export class BlogsService {
    constructor(
        @InjectModel(Blog.name) private blogModel: Model<BlogDocument>,
        private httpService: HttpService
    ) { }

    async findAll(): Promise<Blog[]> {
        return await this.blogModel.find({ display: true }).sort('-created_at').exec();
    }

    async getBatch(): Promise<Blog[]> {
        console.log(`batch job started`)
        return this.httpService.get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
            .pipe(map(res => res.data.hits))
            .toPromise();
    }

    async saveBatch() {
        let duplicates = 0;
        let saved = 0;
        const hits = await this.getBatch();
        for (let hit of hits) {
            const blogSaved = await this.createBlog(hit);
            blogSaved ? saved++ : duplicates++;
        }
        console.log(`batch job finished: ${saved} objects saved - ${duplicates} duplicates ignored `);
    }

    async createBlog(hit: Blog) {
        try {
            const blog = new this.blogModel(hit);
            await blog.save();
            return true;
        } catch (error) {
            if (error.code === 11000) {
                return false;
            }
        }
    }

    async delete(id: string): Promise<Blog> {
        return await this.blogModel.findByIdAndUpdate(id, { display: false });
    }

    @Interval(1000 * 60 * 60)
    handleInterval() {
        this.saveBatch();
    }
}
