import { Controller, Delete, Get, Param } from '@nestjs/common';
import { BlogsService } from './blogs.service';

@Controller('blogs')
export class BlogsController {

    constructor(private blogsService: BlogsService) { }

    @Get()
    findAll() {
        return this.blogsService.findAll();
    }

    @Delete(':id')
    delete(@Param('id') id: string) {
        return this.blogsService.delete(id);
    }

}
