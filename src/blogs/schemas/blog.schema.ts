import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type BlogDocument = Blog & Document & Array<BlogDocument>;

@Schema()
export class Blog {
    @Prop({ unique: true })
    objectID: string;
    @Prop()
    title: string;
    @Prop()
    story_title: string;
    @Prop()
    author: string;
    @Prop()
    created_at: string;
    @Prop()
    story_url: string;
    @Prop()
    url: string;
    @Prop({ default: true })
    display: boolean;
}

export const BlogSchema = SchemaFactory.createForClass(Blog);